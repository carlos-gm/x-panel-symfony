<?php

namespace Panel\Apps\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class NewsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('active', CheckboxType::class, [
                    "label" => "Activar / Inactivar noticia",
                    'required' => false
                ])
                ->add('idNewsCategories', EntityType::class,[ 
                        "class" => 'NewsBundle:NewsCategories',
                        "choice_label" => "name",
                        "label" => "Categoría",
                        "attr" => ["class" => "input-medium"]
                    ]
                )
                ->add('publishDate', DateTimeType::class,[
                    'label' => 'Fecha de publicación',
                    'data' => new \DateTime('now')
                    ]
                )
                ->add('sheduleDate', DateTimeType::class, [
                    'label' => 'Fecha de lanzamiento',
                    'data' => new \DateTime('now')
                    ]
                )
                ->add('title', TextType::class, [
                        'label' => 'Título',
                        'attr' => [ 'id' => 'new_title']
                    ]
                )
                ->add('slug', TextType::class, [
                    'label' => 'Url amiga',
                    'attr' => ['id' => 'new_slug', 'readonly' => true]
                    ]
                )
                ->add('editor', TextType::class, [
                    'label' => 'Autor',
                    'attr'  => ['class' => 'input-inline input-large']
                    ]
                )
                ->add('resume', CKEditorType::class, [
                        'label' => 'Resumen',
                        'config_name' => 'my_config_2'
                    ]
                )
                ->add('content', CKEditorType::class, [
                    'label' => 'Texto de la Noticia',
                    'config_name' => 'my_config_1'
                    ]
                )
                ->add('image', FileType::class, [
                    'label' => 'Imagen principal',
                    'data_class' => null, 
                    'required' => false
                    ]
                )
                ->add('tags', TextType::class,[
                    'label' => 'Tags',
                    'attr'  => ['class' => 'input-large']
                    ]
                )
                /*->add('Guardar', SubmitType::class, [
                    'attr' => ['class' => 'btn green']
                    ]
                )*/;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Panel\Apps\NewsBundle\Entity\News'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'panel_apps_newsbundle_news';
    }


}
