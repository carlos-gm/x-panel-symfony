<?php

namespace Panel\Apps\NewsBundle\Entity;

use Panel\Apps\NewsBundle\Entity\NewsCategories;

/**
 * News
 */
class News
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $idNewsCategories;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $editor;

    /**
     * @var string
     */
    private $resume;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     */
    private $tags;

    /**
     * @var \DateTime
     */
    private $publishDate;

    /**
     * @var DateTime
     */
    private $sheduleDate;

    /**
     * @var bool
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idNewsCategories
     *
     * @param integer $idNewsCategories
     *
     * @return News
     */
    public function setIdNewsCategories($idNewsCategories)
    {
        $this->idNewsCategories = $idNewsCategories;

        return $this;
    }

    /**
     * Get idNewsCategories
     *
     * @return int
     */
    public function getIdNewsCategories()
    {
        return $this->idNewsCategories;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return News
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set editor
     *
     * @param string $editor
     *
     * @return News
     */
    public function setEditor($editor)
    {
        $this->editor = $editor;

        return $this;
    }

    /**
     * Get editor
     *
     * @return string
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * Set resume
     *
     * @param string $resume
     *
     * @return News
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume
     *
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return News
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return News
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set tags
     *
     * @param string $tags
     *
     * @return News
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set publishDate
     *
     * @param \DateTime $publishDate
     *
     * @return News
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * Get publishDate
     *
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * Set setSheduleDate
     *
     * @param \DateTime $sheduleDate
     *
     * @return News
     */
    public function setSheduleDate($sheduleDate)
    {
        $this->sheduleDate = $sheduleDate;

        return $this;
    }

    /**
     * Get sheduleDate
     *
     * @return \DateTime
     */
    public function getSheduleDate()
    {
        return $this->sheduleDate;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return News
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
    /**
     * @var \Panel\Apps\NewsBundle\Entity\NewsCategories
     */
    private $NewsCategories;



    /**
     * Set newsCategories
     *
     * @param \Panel\Apps\NewsBundle\Entity\NewsCategories $newsCategories
     *
     * @return News
     */
    public function setNewsCategories($newsCategories = null)
    {
        $this->NewsCategories = $newsCategories;

        return $this;
    }

    /**
     * Get newsCategories
     *
     * @return \Panel\Apps\NewsBundle\Entity\NewsCategories
     */
    public function getNewsCategories()
    {
        return $this->NewsCategories;
    }
}
