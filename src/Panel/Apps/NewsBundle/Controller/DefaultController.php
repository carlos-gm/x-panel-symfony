<?php

namespace Panel\Apps\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Panel\Apps\NewsBundle\Form\NewsType;
use Panel\Apps\NewsBundle\Entity\News;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    private $session;
        
    public function __construct() {
        $this->session = new Session;
    }
    
    public function getConfig($option){
        $config =  [
            'filePath' => 'uploads/news/imgs',
        ];
        
        return $config[$option];
    }
    
    public function indexAction()
    {
        if(!$this->app_auth('app_news.read')){
            return $this->redirect($this->generateUrl('app'));
        }

        $em     = $this->getDoctrine()->getManager(); 
        $news   = $em->getRepository("NewsBundle:News")->getNews();
        $data   = compact('news');

        return $this->render('NewsBundle:Default:index.html.twig', $data);
    }
    
    public function addAction(Request $request){
        
        
        $new        = new News();
        $form       = $this->createForm(NewsType::class, $new);
        $form->handleRequest($request);
       
        if($form->isSubmitted()){
            
            if($form->isValid()){
                
                $em         = $this->getDoctrine()->getManager(); 
                $cat_repo   = $em->getRepository("NewsBundle:NewsCategories");

                $new = new News;
                
                $cat = $cat_repo->find(
                                $form->get("idNewsCategories")->getData());
                
                $new->setNewsCategories($cat);
                
                $new->setTitle($form->get("title")->getData());
                $new->setSlug($form->get("slug")->getData());
                $new->setEditor($form->get("editor")->getData());
                $new->setResume($form->get("resume")->getData());
                $new->setContent($form->get("content")->getData());
                
                $img = $form["image"]->getData();
                
                if(!empty($img)){
                    
                    $ext = $img->guessExtension();
                    $img_name = time().".".$ext;
                    $img->move($this->getConfig('filePath'), $img_name);

                    $new->setImage($img_name);
                }
                
                $new->setTags($form->get("tags")->getData());
                $new->setPublishDate($form->get("publishDate")->getData());
                $new->setSheduleDate($form->get("sheduleDate")->getData());
                $new->setActive($form->get("active")->getData());
                
                $em->persist($new);
                
                $flush = $em->flush();  
                
                if($flush == null){
                    $status = "La noticia se ha creado correctamente";
                }else{
                    $status = "Error al crear la noticia";
                }
            }else{
                $status = "Error al procesar el formulario";
            }
            
            $this->session->getFlashBag()->add("status", $status);
            return $this->redirectToRoute("app_news");
            
        }else{
            $form_view  = $form->createView();
            $data       = compact('form_view');
            return $this->render('NewsBundle:Default:edit.html.twig', $data);            
        }

    }
    
    public function editAction(Request $request, $id)
    {
        if(!$this->app_auth('app_news.update')){
            die("SIN PERMISOS");
        }
        
        $em     = $this->getDoctrine()->getManager(); 
        
        $new_repo   = $em->getRepository("NewsBundle:News");
        $cat_repo   = $em->getRepository("NewsBundle:NewsCategories");
        
        $new = $new_repo->find($id);
        
        $form       = $this->createForm(NewsType::class, $new);
        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            
            if($form->isValid()){
                
                $cat = $cat_repo->find($form->get("idNewsCategories")->getData());

                //die($form->get("idNewsCategories")->getData());

                $new->setIdNewsCategories($cat->getId());
                $new->setTitle($form->get("title")->getData());
                $new->setSlug($form->get("slug")->getData());
                $new->setEditor($form->get("editor")->getData());
                $new->setResume($form->get("resume")->getData());
                $new->setContent($form->get("content")->getData());
                
                $img = $form["image"]->getData();
                
                if(!empty($img)){
                    
                    $ext = $img->guessExtension();
                    $img_name = time().".".$ext;
                    $img->move($this->getConfig('filePath'), $img_name);

                    $new->setImage($img_name);
                }
                
                $new->setTags($form->get("tags")->getData());
                $new->setPublishDate($form->get("publishDate")->getData());
                $new->setSheduleDate($form->get("sheduleDate")->getData());
                $new->setActive($form->get("active")->getData());
                
                $em->persist($new);
                
                $flush = $em->flush();  
                
                if($flush == null){
                    $status = "Cambios creados correctamente";
                }else{
                    $status = "Error al guardar cambios";
                }
            }else{
                $status = "El formulario no es válido";
            }
            
            $this->session->getFlashBag()->add("status", $status);
        }
        
        $form_view  = $form->createView();
        
        $data   = compact('new','form_view');

        return $this->render('NewsBundle:Default:edit.html.twig', $data);
        
    }
    
    public function deleteAction($id){
        
        $em          = $this->getDoctrine()->getManager(); 
        $new_repo   = $em->getRepository("NewsBundle:News");
        $new        = $new_repo->find($id);

        $em->remove($new);
        $flush = $em->flush();
        
        if(!empty($flush)){
            $status = "Error: No se puede borrar la noticia";
        }else{
            $status = "Noticia borrada correctamente";
        }
        
        $this->session->getFlashBag()->add("status", $status);
        
        return $this->redirectToRoute("app_news");
        
    }
}
