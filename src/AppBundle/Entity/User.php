<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var text
     * @ORM\Column(type="text")
     */
    private $permissions;

    public function getPermissions(){
        return unserialize($this->permissions);
    }
    
    public function setPermissions($permissions){
        $this->$permissions = $permissions;
        return $this;
    }
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    

}